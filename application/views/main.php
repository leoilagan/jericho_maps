<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Project Jericho</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
        <!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
        <!--script src="js/less-1.3.3.min.js"></script-->
        <!--append ‘#!watch’ to the browser URL, then refresh the page. -->

        <link href="<?php echo site_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>css/style.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo site_url(); ?>img/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo site_url(); ?>img/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo site_url(); ?>img/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="img/favicon.png">

        <script type="text/javascript" src="<?php echo site_url(); ?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>js/bootstrap.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
        <script>
            var site_url='<?php echo base_url();?>'
        </script>
    </head>

    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="border-bottom: none !important;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Project Jericho</a>
                </div>
                <div class="navbar-collapse collapse navbar-ex1-collapse">
                    <div class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                        <li class="dropdown user-dropdown"><a href="#" data-toggle="dropdown">Welcome <b>admin</b> <b class="caret"></b></a>
                            <ul class="dropdown-menu">

                                <li><a href="#"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
                            </ul>
                        </li>
                    </div>
                </div><!--/.navbar-collapse -->
            </div>
        </div>


        <div class="container" style="margin-top: 30px">
            <br><br>
            <div class="row clearfix">
                <div class="col-md-3 column">
                    <div class="panel panel-default">
                        <div class="panel-heading">Blind Info</div>
                        <div class="panel-body">
                            <img alt="140x140" src="<?php echo site_url(); ?>profiles/<?php echo $user_info->avatar; ?>" class="img-circle" style="height: 140px;width: 140px;">
                            <br>
                            <input type="hidden" id="user_id"  name="user_id" value="<?php echo $user_info->id;?>">
                            Name: <?php echo $user_info->first_name . " " . $user_info->last_name; ?>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Devices</div>
                        <div class="panel-body">

                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">Sessions</div>
                        <div class="panel-body">
                            <select class="btn btn-block dropdown-toggle" id="sessionlist" title="List">
                                <?php echo $session_list; ?>
                            </select>

                        </div>
                    </div>


                </div>
                <div class="col-md-9 column">
                    <div class="panel panel-default">
                        <div class="panel-heading">Tracks</div>
                        <div class="panel-body" id="map-canvas" style="height:400px; margin: 0px; padding: 0px;">

                        </div>
                    </div>

                </div>
            </div>
            <br>
            <br>
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <div class="panel panel-default">
                        <div class="panel-heading">Movement Logs</div>
                        <div class="panel-body" >
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>

                                        <th>
                                            Longitude
                                        </th>
                                        <th>
                                            Latitude
                                        </th>
                                        <th>
                                            Time
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="session_body">
                                    <?php foreach ($movements as $movement): ?>
                                        <tr>

                                            <td>
                                                <?php echo $movement->longitude; ?>
                                            </td>
                                            <td>
                                                <?php echo $movement->latitude; ?>
                                            </td>
                                            <td>
                                                <?php echo $movement->date_created; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>
     <script type="text/javascript" src="<?php echo site_url(); ?>js/scripts.js"></script>

</html>
