<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Mod_api extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * it will return true when the given $facebook_id exist on the db
     * @param type $facebook_id
     */
    function check_user_exist($facebook_id) {
        $this->db->where('facebook_id', $facebook_id);
        $result = $this->db->get('user')->result();
        if ($result) {
            return $result[0];
        }
        return false;
    }

    
    function fetch_movements($user_id,$session_id){
        $this->db->where('user_id', $user_id);
        $this->db->where('session_id', $session_id);
        $this->db->from('movements');
        $query = $this->db->get();
        return $query->result();
    }
    
    function fetch_session($user_id) {
        $this->db->select('session_id,date_created');
        $this->db->where('user_id', $user_id);
        $this->db->group_by("session_id");
        
        $this->db->from('movements');
        
        $query = $this->db->get();
        return $query->result();
    }
    
    function fetch_userinfo($user_id){
         $this->db->where('id', $user_id);
        $result = $this->db->get('user')->result();
        if ($result) {
            return $result[0];
        }
        return false;
    }
    
    function fetch_device($user_id){
         $this->db->where('id', $user_id);
        $result = $this->db->get('user')->result();
        if ($result) {
            return $result[0];
        }
        return false;
    }
    

    function insert_movement($data) {
        return $this->db->insert('movements', $data);
    }

}
