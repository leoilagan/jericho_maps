<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author shinji
 */
class Login extends CI_Controller {
  public function __construct() {
        parent::__construct();
        $this->load->model('mod_login');
     //   $this->load->library('session');
     
    }

    //check username and password on database
    public function authenticate() {
      
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);
        //authenticate users to db
        $accounts = $this->mod_login->authenticate(
                $username, $password
        );
     
        //check if results have been found
        if (count($accounts) > 0) {
            //set the user session data
            $this->load->library('session');
            
            $this->session->set_userdata(array(
                'user_id' => $accounts[0]->id,
                'username' => $username,
                'first_name' => $accounts[0]->first_name,
                'last_name' => $accounts[0]->last_name,
                'status'=> $accounts[0]->status,
                'isLoggedIn' => true
            )
            );
             $this->successful();
        }
        else
            $this->failed('Invalid username/password.');
    }
    
   

    //attempt to login was successful
    private function successful() {
        redirect(base_url('schedule'));
        
    }

    private function failed($msg) {
        $this->load->library('session');
        $this->session->sess_destroy();
    //      $this->form_validation->resetpostdata();
        $data['project_title'] = 'Pasig Ferry App CMS';
        $data['errorMsg'] = $msg;
        $data['reset'] = TRUE;
        $this->load->view('login.php', $data);
    }

}
