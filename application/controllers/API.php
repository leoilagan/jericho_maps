<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class API extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mod_api');
    }

    public function index() {
        echo 'test';
    }

    
    private function _check_user_exist($facebook_id){
         $result = $this->mod_api->check_user_exist($facebook_id);
           
    }

    private function _message_encoded($message, $status) {
        header('Content-Type: application/json');

        echo json_encode(array("response_code" => $status, "message" => $message));
    }

    public function test(){
        echo 'test';
    }
    
    public function movements(){
         $data = $this->input->get();
          $r_data = array();
         $r_data['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
         $r_data['session_id'] = isset($data['session_id']) ? $data['session_id'] : '';
         $r_data['latitude'] = isset($data['latitude']) ? $data['latitude'] : '';
         $r_data['longitude'] = isset($data['longitude']) ? $data['longitude'] : '';
         $r_data['device_id'] = isset($data['device_id']) ? $data['device_id'] : '';
     echo    $this->mod_api->insert_movement($r_data);
    }
    
}
