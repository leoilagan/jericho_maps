<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of main
 *
 * @author shinji
 */
class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_api');
        $this->load->helper('url');
    }

    
    
    
    public function index() {
         // $this->load->view('main');  
      $user_info = $this->mod_api->fetch_userinfo('1');
      $data['user_info'] = $user_info;
      $data['session_list'] = $this->load_session('1', 0);
      $data['movements'] = $this->mod_api->fetch_movements('1','uRcwCU9');
     // $user_devices = 
      $this->load->view('main', $data);
    }

    public function show_login($isErrorMsg = false) {
        $data['project_title'] = 'Pasig Ferry App CMS';
        if ($isErrorMsg)
            $data['errorMsg'] = '';
        $data['reset'] = TRUE;
        $this->load->view('login', $data);
    }
    
    
    public function movements(){
        $data = $this->input->get();
        $user_id = $data['user_id'];
        $session_id = $data['session_id'];
     $result =   $this->mod_api->fetch_movements($user_id,$session_id);
     echo json_encode($result);
    }
    
    public function load_session($user_id,$selected=0){
        $html = '';
       $session_list = $this->mod_api->fetch_session($user_id);  
       $i=0;
       foreach ($session_list as $session){
        $html.= '<option';
            if($i ==$selected){
                $html.=' selected';
            }
            $html.= '>' . $session->session_id;

            $html .="</option>";
            $i++;
        }
        return $html;
    }

}
