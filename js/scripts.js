
google.maps.event.addDomListener(window, 'load', initialize);

var map;
var movementPath;
function initialize() {
    var user_id = $('#user_id').val();
    var session_id = $("#sessionlist").val();

    fetch_movements(user_id, session_id);

}


$("#sessionlist").change(function () {
    var user_id = $('#user_id').val();
    var session_id = $(this).val();
    fetch_movements(user_id, session_id);

});


function addLine() {
    movementPath.setMap(map);
}

function removeLine() {
    movementPath.setMap(null);
}

function fetch_movements(user_id, session_id) {
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: site_url + 'main/movements',
        data: {
            user_id: user_id,
            session_id: session_id
        },
        success: function (data) {
            size = data.length;
             midpointIndex = 0;
            if(size<2) 
                midpointIndex = size / 2;
          
            midLatitude = data[midpointIndex].latitude;
            midLongitude = data[midpointIndex].longitude;
            var mapOptions = {
                zoom: 22,
                center: new google.maps.LatLng(midLatitude, midLongitude)
            };
            map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);
            var coordinates = [];
            var htmlStr = '';
              $("#session_body").empty();
            $.each(data, function (i, item) {

                coordinates.push(new google.maps.LatLng(item.latitude, item.longitude));
                htmlStr += "<tr><td>" + item.longitude + "</td><td>" + item.latitude + "</td><td>" + item.date_created + "</td></tr>";


            });
            $("#session_body").append(htmlStr)
            movementPath = new google.maps.Polyline({
                path: coordinates,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });

            addLine();

        },
        error: function (xhr, status, error) {
        }
    });
}

